import importlib
import math
import bpy
import sympy
from typing import List

from blender_node_expr.utils import AttrPath
from blender_node_expr.expr import BlNode, BlExpr, BlConst, BlVariable
from blender_node_expr.make_nodes import new_node_group, SocketSpec, RichNodeGroup, NodeGroupCompiler, BasicAutoroute
from blender_sympy.converter import Converter

from blender_sympy_common.coords import SphericalCoordinates
from blender_sympy_common.complex_scalar_field import ComplexScalarField

class GeometryNodesSetMaterial:
	def __init__(self, name = 'Set Material'):
		self.name = name

	def make_node_group(self) -> RichNodeGroup:
		inputs = [SocketSpec("Geometry", 'Geometry'), SocketSpec("Material", 'Material')]
		outputs = [SocketSpec("Geometry", 'Geometry')]

		node_group = new_node_group('Geometry', self.name, inputs, outputs)
		comp = NodeGroupCompiler(node_group, {})

		expr = BlNode('GeometryNodeSetMaterial', {}, [
			BlVariable("Geometry"),
			BlConst(None),
			BlVariable("Material")
		]).output(0)

		comp.compile_expr_into(expr, "Geometry")

		BasicAutoroute(node_group.inner).arrange()

		return node_group

r = sympy.Symbol("Radius", positive=True)
inc = sympy.Symbol("Inclination", nonnegative=True) # 0 <= inc <= pi
azi = sympy.Symbol("Azimuth", real=True)  # -pi < azi <= pi

class ScaledSphericalCoordinates(SphericalCoordinates):
	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)

	def parameters(self) -> List[SocketSpec]:
		return [SocketSpec("Scale", 'Float')]

	def to_bl_exprs(self) -> List[BlExpr]:
		coords = super().to_bl_exprs()

		coords[0] = Converter().quick_node(sympy.Mul, [coords[0], BlVariable("Scale")])

		return coords

sph = ScaledSphericalCoordinates()

fac = sympy.Symbol("Factor", positive=True)
t = sympy.Symbol("Time", real=True)

input_repls = {
	"Scale": BlNode('ShaderNodeAttribute', { AttrPath('attribute_type'): 'OBJECT', AttrPath('attribute_name'): "[\"Scale\"]" }, []).output(2),
	"Factor": BlNode('ShaderNodeAttribute', { AttrPath('attribute_type'): 'OBJECT', AttrPath('attribute_name'): "[\"Factor\"]" }, []).output(2),
	"Time": BlNode('ShaderNodeAttribute', { AttrPath('attribute_type'): 'OBJECT', AttrPath('attribute_name'): "[\"Time\"]" }, []).output(2),
	"Max Density": BlConst(100)
}

h = importlib.import_module('.physics.hydrogen', 'sympy')
orb = lambda n,l,m: sympy.exp(-sympy.I * h.E_nl_dirac(n, l) * t) * h.Psi_nlm(n, l, m, r, azi, inc)

w_2s0 = orb(2,0,0)
w_2pz = orb(2,1,0)
w_2px = -(orb(2,1,1) - orb(2,1,-1))
w_2py = -(orb(2,1,1) + orb(2,1,-1))

w_2sp2_xz = sympy.sqrt(2) * w_2s0 - w_2pz + sympy.sqrt(3) * w_2px
w_2sp3_1 = w_2s0 + w_2px + w_2py + w_2pz

w_test = orb(5,3,1) + orb(3,2,1)

csf = ComplexScalarField(fac * w_test)

make_geometry = True

if make_geometry:
	geo = csf.to_geometry_nodes("Test Orbital", sph, max_density=100, group_inputs = [SocketSpec("Scale", 'Float'), SocketSpec("Factor", 'Float'), SocketSpec("Time", 'Float')], max_nodes_per_layer = 10)
	mat = csf.to_material("SurfaceWithAlpha", "Test Orbital Surface Shader", sph, non_coord_inputs = input_repls, max_nodes_per_layer = 10)
	geo_set_mat = GeometryNodesSetMaterial().make_node_group()
else:
	mat = csf.to_material("Volume", "Test Orbital Volume Shader", sph, non_coord_inputs = input_repls, max_nodes_per_layer=10)

bpy.ops.mesh.primitive_cube_add(size=10, scale=(1, 1, 1))
bpy.data.objects['Light'].data.type = 'SUN'

if make_geometry:
	bpy.data.objects['Light'].data.energy = 10

ob = bpy.data.objects["Cube"]
ob["Scale"] = 1.0
ob["Factor"] = 10.0
ob["Time"] = 0.0

if not make_geometry:
	bpy.data.objects['Cube'].active_material = mat
else:
	mf = bpy.data.objects['Cube'].modifiers.new("Geometry Nodes", 'NODES')
	mf.node_group = geo.inner

	def driver(a, b):
		dr = mf.driver_add(f"[\"{a}\"]")
		dr.driver.type = 'MAX'
		var = dr.driver.variables.new()
		var.targets[0].id = ob
		var.targets[0].data_path = f"[\"{b}\"]"

	driver("Input_1", "Scale")
	driver("Input_2", "Factor")
	driver("Input_3", "Time")

	mf = bpy.data.objects['Cube'].modifiers.new("Set Material", 'NODES')
	mf.node_group = geo_set_mat.inner
	mf["Input_1"] = mat