import bpy
import math
import sympy

from sympy.core import function as sympy_function, numbers as sympy_numbers, relational as sympy_relational
from . import sympy_ext
from blender_node_expr.utils import AttrPath
from blender_node_expr.expr import BlNode, BlExpr, BlConst, BlVariable

class Converter:
	# singleton
	def __new__(cls):
		if not hasattr(cls, 'instance'):
			cls.instance = super(Converter, cls).__new__(cls)
		return cls.instance

	def __init__(self):
		m = lambda op, ex=[]: ('ShaderNodeMath', {AttrPath('operation'): op}, ex)

		# special constants that are handled differently from functions/operations
		self.special_const_map = {
			'Exp1': math.e,
			'Pi': math.pi,
			'NaN': math.nan,
			'Infinity': math.inf,
			'NegativeInfinity': -math.inf
		}

		# associative operations that need to be binarized first
		self.assoc_math_func_map = {
			'Add': ('ADD', 0),
			'Mul': ('MULTIPLY', 1),
			'And': ('MULTIPLY', 1),
			'Or': ('ADD', 0),
			'Min': ('MINIMUM', sympy.oo),
			'Max': ('MAXIMUM', -sympy.oo)
		}

		# regular sympy functions and operations
		self.func_map = {
			'Not': m('SUBTRACT', [(0, 1)]),
			'Pow': m('POWER'),
			'log': m('LOGARITHM', [(1, sympy.E)]),
			'sqrt': m('SQRT'),
			'Abs': m('ABSOLUTE'),
			'exp': m('EXPONENT'),
			'StrictLessThan': m('LESS_THAN'),
			'StrictGreaterThan': m('GREATER_THAN'),
			'sign': m('SIGN'),
			'Equality': m('COMPARE', [(2, 0.001)]),
			'floor': m('FLOOR'),
			'ceiling': m('CEIL'),
			# sympy's `Mod` is Mod(x, m) = x - m * floor(x/m)
			# while blender's `MODULO` is MODULO(x, m) = x - m * trunc(x/m).
			# Blender's `WRAP` with `min=0` is exactly what's needed instead.
			'Mod': m('WRAP', [(2, 0)]),
			'sin': m('SINE'),
			'cos': m('COSINE'),
			'tan': m('TANGENT'),
			'asin': m('ARCSINE'),
			'acos': m('ARCCOSINE'),
			'atan': m('ARCTANGENT'),
			'atan2': m('ARCTAN2'),
			'sinh': m('SINH'),
			'cosh': m('COSH'),
			'tanh': m('TANH'),
		}

		# custom functions, encoded in sympy as undefined functions with a special name
		self.custom_func_map = {
			### Make all blender math nodes accessible ###
			'Sub': m('SUBTRACT'),
			'Div': m('DIVIDE'),
			'MulAdd': m('MULTIPLY_ADD'),
			'Log': m('LOGARITHM'),
			'InvSqrt': m('INVERSE_SQRT'),
			'AlmostEqual': m('COMPARE'),
			'SmoothMin': m('SMOOTH_MIN'),
			'SmoothMax': m('SMOOTH_MAX'),
			'round': m('ROUND'),
			'trunc': m('TRUNC'),
			'fract': m('FRACT'),
			'ModTrunc': m('MODULO'),
			# Wrap(x, max, min) = Wrap(x - min, max - min, 0) + min
			# where Wrap(x, m, 0) = x - m * floor(x/m)
			'Wrap': m('WRAP'),
			# Snap(x, inc) = inc * floor(x/inc) = x - Mod(x, inc)
			'Snap': m('SNAP'),
			# positive triangle wave
			# PingPong(x, scale) = scale - |scale - Mod(x, 2 * scale)|
			'PingPong': m('PINGPONG'),
			'DegToRad': m('RADIANS'),
			'RadToDeg': m('DEGREES')
			### other custom functions ###
			# (none yet)
		}

	def convert(self, f) -> BlExpr:
		if isinstance(f, int):
			f = sympy.Integer(f)
		elif isinstance(f, float):
			f = sympy.Float(f)

		if not any(isinstance(f, T) for T in [sympy.Expr, sympy_relational.Relational]):
			raise TypeError(f'cannot convert non-expression to BlExpr: {f}')

		# unequality has to become two nodes, so the transformation is done up front
		if isinstance(f, sympy.Unequality):
			f = sympy.Not(sympy.Eq(*f.args), evaluate=False)

		func = f.func.__name__
		args = f.args

		if isinstance(f, sympy.Symbol):
			return BlVariable(f.name)
		elif isinstance(f, sympy.Number) or isinstance(f, sympy.NumberSymbol):
			for k, v in self.special_const_map.items():
				if isinstance(f, getattr(sympy_numbers, k)):
					return BlConst(v)

			if isinstance(f, sympy.Rational):
				# this also takes care of sympy.Integer, which is a subclass of sympy.Rational
				return BlConst(f.p / f.q)
			elif isinstance(f, sympy.Float):
				return BlConst(float(f))
		elif isinstance(f, sympy_ext.IversonBracket):
			# the iverson bracket is transparent to blender's nodes
			# because they auto-convert booleans to numbers
			return self.convert(f.args[0])
		elif func in self.assoc_math_func_map:
			operation, empty_value = self.assoc_math_func_map[func]

			# these first two cases should not happen
			# because sympy simplifies all that stuff away
			# but it's implemented anyway, just in case
			if len(args) == 0:
				return BlConst(empty_value)
			elif len(args) == 1:
				return self.convert(args[0])
			else:
				lhs = self.convert(args[0])
				rest = args[1:]
				while len(rest) != 0:
					rhs = self.convert(rest[0])
					rest = rest[1:]
					tmp = BlNode('ShaderNodeMath', {AttrPath('operation'): operation}, [lhs, rhs])
					lhs = tmp.output(0)
				return lhs
		else:
			# use separate lookups depending on the type of `f`
			if isinstance(f.func, sympy_function.UndefinedFunction):
				# custom function
				func_map = self.custom_func_map
				err_msg = 'no such custom function'
			else:
				# regular sympy-defined function
				func_map = self.func_map
				err_msg = 'unsupported function'

			if func not in func_map:
				raise ValueError(f'{err_msg}: {func}')

			kind, props, extra_args = func_map[func]

			inputs = [self.convert(arg) for arg in args]
			for i, arg in extra_args:
				inputs.insert(i, self.convert(arg))

			out_node = BlNode(kind, props, inputs)
			return out_node.output(0)

	def quick_node(self, op, args: list) -> BlExpr:
		# pass a string as `op` to use a custom operation
		if type(op) is str:
			op = sympy.Function(op)

		# make as many dummy inputs to the function as there are args
		dummy_names = [f"dummy{i}" for i in range(len(args))]
		dummies = [sympy.Symbol(n) for n in dummy_names]

		# convert the function call and then substitute the given args for the dummies
		dummy_subs = dict(zip(dummy_names, args))
		return self.convert(op(*dummies)).subs(dummy_subs)

