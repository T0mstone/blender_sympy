import sympy
from sympy.logic import boolalg as sympy_bool

class IversonBracket(sympy.Function):
	"""
	Boolean to number conversion: true -> 1, false -> 0
	"""
	@classmethod
	def eval(cls, x):
		if not isinstance(x, sympy_bool.Boolean):
			raise TypeError('Iverson bracket is only defined on boolean values')
		if isinstance(x, sympy_bool.BooleanTrue):
			return sympy.S.One
		elif isinstance(x, sympy_bool.BooleanFalse):
			return sympy.S.Zero

def simplified_atan2(y, x):
	if not (sympy.ask(sympy.Q.real(x)) and sympy.ask(sympy.Q.real(y))):
		raise ValueError('complex arguments to simplified_atan2')

	if x == 0:
		return (sympy.sign(y) * sympy.pi / 2).simplify()
	elif y == 0:
		x_lt0 = (sympy.sign(x) < 0).simplify()
		# a sign function can be removed in a comparison with 0
		if isinstance(x_lt0, sympy.StrictLessThan) \
			and isinstance(x_lt0.lhs, sympy.sign):
			x_lt0 = x_lt0.lhs.args[0] < 0

		return IversonBracket(x_lt0) * sympy.pi

	# this should keep only having one I because x and y are real
	z = sympy.factor_terms(x + sympy.I * y)

	if isinstance(z, sympy.Mul):
		# separate the inner expression from the prefactors
		prefactors = []
		inner = None
		for arg in z.args:
			is_inner = False
			if isinstance(arg, sympy.Add):
				for summand in arg.args:
					if summand == sympy.I or isinstance(summand, sympy.Mul) and sympy.I in summand.args:
						is_inner = True
						break
			if is_inner:
				inner = arg
			else:
				prefactors += [arg]
		assert inner is not None

		# from the prefactors, only the sign is important
		fac = sympy.sign(sympy.Mul(*prefactors)).simplify()

		# note: `re` and `im` here don't need to be simplified
		# because they were made from a simple `x + I y` to begin with (see above, def. of `z`)
		re = fac * sympy.re(inner)
		im = fac * sympy.im(inner)

		return sympy.atan2(im, re)
	else:
		# no factors were extracted
		# there's nothing more we can do
		return sympy.atan2(y, x)

class ComplexTools:
	def __init__(self, z):
		self.z = z

	def simplified_real_part(self):
		return sympy.re(self.z).simplify()

	def simplified_imaginary_part(self):
		return sympy.im(self.z).simplify()

	def simplified_modulus_squared(self):
		return (sympy.Abs(self.z) ** 2).expand(complex = True).simplify()

	def simplified_arg(self):
		res = sympy.arg(self.z).simplify().expand(complex=True)
		if isinstance(res, sympy.arg):
			z = res.args[0]
			res = simplified_atan2(sympy.im(z), sympy.re(z))
		return res