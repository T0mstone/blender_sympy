from typing import List, Optional
from bpy.types import NodeTree
import sympy

from blender_node_expr.utils import AttrPath
from blender_node_expr.expr import BlNode, BlExpr, BlVariable
from blender_node_expr.make_nodes import new_node_group, SocketSpec, RichNodeGroup, NodeGroupCompiler, BasicAutoroute
from blender_sympy.converter import Converter

class CoordinateSystem:
	def __init__(self, name: str):
		self.name = name
		self.cached_outputs = {}

	def parameters(self) -> List[SocketSpec]:
		return []

	def coordinate_names(self) -> List[str]:
		return []

	def to_bl_exprs(self) -> List[BlExpr]:
		return []

	def make_node_group(self, kind: str, suffix_name: bool = True, max_nodes_per_layer: Optional[int] = None, force_new: bool = False) -> RichNodeGroup:
		if not force_new and (self.name, kind) in self.cached_outputs:
			return self.cached_outputs[(self.name, kind)]

		if suffix_name:
			self.name += f" ({kind})"
		inputs = [SocketSpec("Position", 'Vector')] + self.parameters()
		outputs = [SocketSpec(name, 'Float') for name in self.coordinate_names()]

		node_group = new_node_group(kind, self.name, inputs, outputs)
		comp = NodeGroupCompiler(node_group, {})

		for i, expr in enumerate(self.to_bl_exprs()):
			comp.compile_expr_into(expr, outputs[i].name)

		BasicAutoroute(node_group.inner).arrange(max_per_layer = max_nodes_per_layer)

		self.cached_outputs[(self.name, kind)] = node_group

		return node_group

	def logical_group_node(self, kind: str, position_expr: BlExpr, *args, **kwargs) -> BlNode:
		node_group = self.make_node_group(kind, *args, **kwargs)

		param_vars = [BlVariable(param.name) for param in self.parameters()]

		return BlNode(f'{kind}NodeGroup', { AttrPath('node_tree'): node_group.inner }, [ position_expr ] + param_vars)

class SphericalCoordinates(CoordinateSystem):
	def __init__(self, name="Spherical Coordinates"):
		super().__init__(name)

	def coordinate_names(self) -> List[str]:
		return ["Radius", "Inclination", "Azimuth"]

	def to_bl_exprs(self) -> List[BlExpr]:
		position = BlVariable("Position")
		conv = Converter()

		xyz = BlNode('ShaderNodeSeparateXYZ', {}, [position])
		rad = BlNode('ShaderNodeVectorMath', {AttrPath('operation'): 'LENGTH'}, [position])

		x,y,z = tuple(xyz.output(i) for i in range(3))
		# vector length has a hidden first (index 0) output
		r = rad.output(1)

		incl = conv.quick_node(sympy.acos, [conv.quick_node('Div', [z, r])])
		azim = conv.quick_node(sympy.atan2, [y, x])

		return [r, incl, azim]