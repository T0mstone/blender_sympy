import bpy
import math
import sympy
from bpy.types import Material
from typing import List, Dict, Optional, Tuple, Union

from blender_node_expr.utils import AttrPath
from blender_node_expr.expr import BlNode, BlExpr, BlConst, BlVariable
from blender_node_expr.make_nodes import new_node_group, SocketSpec, RichNodeGroup, NodeGroupCompiler, BasicAutoroute
from blender_sympy.sympy_ext import ComplexTools
from blender_sympy.converter import Converter
from .coords import CoordinateSystem

Number = Union[int, float]

class ComplexScalarField:
	def __init__(self, complex_formula):
		self.inner = ComplexTools(complex_formula)
		self.cached_outputs = {}

	def expr_real_part(self, coords: Dict[str, BlExpr]) -> BlExpr:
		formula = self.inner.simplified_real_part()
		return Converter().convert(formula).subs(coords)

	def expr_imaginary_part(self, coords: Dict[str, BlExpr]) -> BlExpr:
		formula = self.inner.simplified_imaginary_part()
		return Converter().convert(formula).subs(coords)

	def expr_modulus_squared(self, coords: Dict[str, BlExpr]) -> BlExpr:
		formula = self.inner.simplified_modulus_squared()
		return Converter().convert(formula).subs(coords)

	def expr_arg(self, coords: Dict[str, BlExpr]) -> BlExpr:
		formula = self.inner.simplified_arg()
		return Converter().convert(formula).subs(coords)

	def expr_arg_as_color(self, coords: Dict[str, BlExpr]) -> BlExpr:
		arg = self.expr_arg(coords)

		mapped = BlNode('ShaderNodeMapRange', {}, [
			arg,
			BlConst(-math.pi),
			BlConst(math.pi),
		])

		res = BlNode('ShaderNodeCombineHSV', {}, [
			mapped.output(0),
			BlConst(1),
			BlConst(1)
		])

		return res.output(0)

	def _to_material_expr(self, mode: str, coordinate_system: CoordinateSystem, max_nodes_per_layer: Optional[int]) -> Tuple[BlExpr, int]:
		# 'Geometry' node where we get the position from
		n_geo = BlNode('ShaderNodeNewGeometry', {}, [])
		expr_position = n_geo.output(0)

		coord_sys_group = coordinate_system.logical_group_node('Shader', expr_position, max_nodes_per_layer = max_nodes_per_layer)
		coords = { name: coord_sys_group.output(i) for i, name in enumerate(coordinate_system.coordinate_names()) }

		expr_arg_col = self.expr_arg_as_color(coords)

		if mode == "Volume":
			expr_mod_sqr = self.expr_modulus_squared(coords)

			n_sh = BlNode('ShaderNodeVolumePrincipled', {}, [
				expr_arg_col,
				BlConst(None),
				expr_mod_sqr
			])

			mat_out_index = 1
		elif mode == "Surface":
			n_sh = BlNode('ShaderNodeBsdfPrincipled', {}, [expr_arg_col])

			mat_out_index = 0
		elif mode == "SurfaceWithAlpha":
			expr_mod_sqr = self.expr_modulus_squared(coords)

			expr_scaled_density = BlNode('ShaderNodeMapRange', { AttrPath('clamp'): True }, [
				expr_mod_sqr,
				BlConst(0),
				BlVariable("Max Density"),
			]).output(0)

			n_sh1 = BlNode('ShaderNodeBsdfPrincipled', {}, [expr_arg_col])
			n_sh2 = BlNode('ShaderNodeBsdfTransparent', {}, [])

			n_sh = BlNode("ShaderNodeMixShader", {}, [expr_scaled_density, n_sh1.output(0), n_sh2.output(0)])

			mat_out_index = 0
		result = n_sh.output(0)

		return result, mat_out_index

	def to_material(self, mode: str, name: str, coordinate_system: CoordinateSystem, *, non_coord_inputs: Dict[str, BlExpr] = {}, max_nodes_per_layer: Optional[int] = None, force_new: bool = False) -> Material:
		if not force_new and ("Shader", name) in self.cached_outputs:
			return self.cached_outputs[("Shader", name)]

		mat = bpy.data.materials.new(name)
		mat.use_nodes = True

		sha = mat.node_tree

		sha.links.clear()
		sha.nodes.clear()

		node_material_output = sha.nodes.new('ShaderNodeOutputMaterial')

		result_expr, mat_out_index = self._to_material_expr(mode, coordinate_system, max_nodes_per_layer)
		result_expr = result_expr.subs(non_coord_inputs)

		sha = RichNodeGroup(sha, {}, { "Output": node_material_output.inputs[mat_out_index] })
		comp = NodeGroupCompiler(sha, {})

		result_node = comp.compile_expr_into(result_expr, "Output")

		BasicAutoroute(sha.inner).arrange(max_per_layer = max_nodes_per_layer)

		self.cached_outputs[("Shader", name)] = mat

		return mat

	def _to_geometry_expr(self, coordinate_system: CoordinateSystem, has_max_density: bool, max_nodes_per_layer: Optional[int]) -> BlExpr:
		n_pos = BlNode('GeometryNodeInputPosition', {}, [])
		expr_position = n_pos.output(0)

		coord_sys_group = coordinate_system.logical_group_node('Geometry', expr_position, max_nodes_per_layer = max_nodes_per_layer)
		coords = { name: coord_sys_group.output(i) for i, name in enumerate(coordinate_system.coordinate_names()) }

		expr_mod_sqr = self.expr_modulus_squared(coords)
		expr_arg_col = self.expr_arg_as_color(coords)

		if has_max_density:
			expr_density = Converter().quick_node(sympy.Min, [expr_mod_sqr, BlVariable("Max Density")])
		else:
			expr_density = expr_mod_sqr

		expr_resolution = BlNode('ShaderNodeValue', { AttrPath('label'): "Resolution", AttrPath('outputs', 0, 'default_value'): 32 }, []).output(0)

		# Blender 3.3 only
		n_vc = BlNode('GeometryNodeVolumeCube', {}, [
			expr_density,
			BlConst(None),
			BlNode('GeometryNodeBoundBox', {}, [BlVariable("Geometry")]).output(1),
			BlNode('GeometryNodeBoundBox', {}, [BlVariable("Geometry")]).output(2),
			expr_resolution,
			expr_resolution,
			expr_resolution,
		])

		n_mesh = BlNode('GeometryNodeVolumeToMesh', {}, [n_vc.output(0)])

		return n_mesh.output(0)

	def to_geometry_nodes(self, name: str, coordinate_system: CoordinateSystem, *, group_inputs: List[SocketSpec] = [], input_replacements: Dict[str, BlExpr] = {}, max_density: Optional[Number] = None, max_nodes_per_layer: Optional[int] = None, force_new: bool = False) -> RichNodeGroup:
		"""
		This function only works on Blender 3.3 or newer
		"""
		if not force_new and ("Geometry", name) in self.cached_outputs:
			return self.cached_outputs[("Geometry", name)]

		inputs = [SocketSpec("Geometry", 'Geometry')] + group_inputs
		outputs = [SocketSpec("Geometry", 'Geometry')]

		has_max_density = max_density is not None
		if has_max_density:
			input_replacements["Max Density"] = BlConst(max_density)
		output_expr = self._to_geometry_expr(coordinate_system, max_density is not None, max_nodes_per_layer)
		output_expr = output_expr.subs(input_replacements)

		node_group = new_node_group('Geometry', name, inputs, outputs)
		comp = NodeGroupCompiler(node_group, {})

		comp.compile_expr_into(output_expr, "Geometry")

		BasicAutoroute(node_group.inner).arrange(max_per_layer = max_nodes_per_layer)

		self.cached_outputs[("Geometry", name)] = node_group

		return node_group