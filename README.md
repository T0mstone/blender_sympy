# SymPy to Blender conversion

Don't want to hassle around with nodes for complicated formulas? Me neither.

With this, you can just write your formula out in SymPy and let it be converted to nodes automatically!

## Installation

This isn't an add-on but a library to be used by add-ons or scripts.

As a consequence, the installation process is a tiny bit more involved than just importing an add-on:
You'll need to install this package and its dependencies (which should be taken care of automatically)
into a location that blender recognizes.

There are multiple possibilities for this but the most correct location for these kinds of things is the `modules/` directory in blender's user scripts directory (which is usually `~/.local/blender/<version>/scripts/` on Linux).

The easiest way to do that is to copy the following script into blender's script editor and run it.
Do note that blender will become unresponsive (HANG) while pip is doing its job.
```python
import bpy, sys, subprocess
from pathlib import Path

python = sys.executable
install_dir = Path(bpy.utils.script_path_user()) / "modules"
# install and update pip for the user
# (pip will be installed into the user site-pakages directory for the python version, which is independent of blender)
subprocess.call([python, "-m", "ensurepip", "--user"])
subprocess.call([python, "-m", "pip", "install", "--user", "--upgrade", "pip"])
# install the packages
subprocess.call([python, "-m", "pip", "install", f"--target={str(install_dir)}", "git+https://codeberg.org/T0mstone/blender_sympy"])
```
Then you may need to restart blender, but after that you can type
```python
import sympy, blender_sympy, blender_sympy_common
```
in blender's python console and if there's no error, the installation was successful.

## Updating

If you want to update this library or if you have already set up blender's `pip` in the past,
you can use the following short version of the install script:
```python
import bpy, sys, subprocess
from pathlib import Path
python = sys.executable
install_dir = Path(bpy.utils.script_path_user()) / "modules"
subprocess.call([python, "-m", "pip", "install", "--upgrade", f"--target={str(install_dir)}", "git+https://codeberg.org/T0mstone/blender_sympy"])
```

## Example

An example script that I've used to play around can be found in `example.py`.